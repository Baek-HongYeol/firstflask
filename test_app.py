import app as srv
import unittest

class TestFlaskApi(unittest.TestCase):
    def setUp(self):
        self.app = srv.app.test_client()
    
    def test_add(self):
        a = 6
        b = 21
        query_string = "a="+str(a)+"&b="+str(b)
        response = self.app.get('/add', query_string=query_string)
        print(response)
        self.assertEqual(response.get_data(), bytes(str(a+b), encoding="ascii"))
    
    def test_sub(self):
        a = 92
        b = 15
        query_string = "a="+str(a)+"&b="+str(b)
        response = self.app.get('/sub', query_string=query_string)
        print(response)
        self.assertEqual(response.get_data(), bytes(str(a-b), encoding="ascii"))
    
if __name__ == '__main__':
    unittest.main()