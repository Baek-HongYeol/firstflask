from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello Bob from xxxxx'

@app.route('/add', methods=['GET'])
def add():
    temp = request.args.get('a', 1)
    temp1 = request.args.get('b', -1)
 
    return str(int(temp) + int(temp1))

@app.route('/sub', methods=['GET'])
def sub():
    temp = request.args.get('a', 0)
    temp1 = request.args.get('b', -1)

    return str(int(temp) - int(temp1))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081)


